source $HOME/.config/general/aliases
source $HOME/.config/general/enviroment
source $HOME/.config/general/prompt

if [ -z "$DISPLAY" ]; then
	export LANG=en_US.UTF-8
	export LC_COLLATE=C
fi

if [ -f "$HOME/.customrc" ]; then
	source "$HOME/.customrc"
fi

GPG_TTY=$(tty)
export GPG_TTY

autoload -Uz compinit
compinit
zstyle ':completition:*' menu select
zstyle ':completion:*' matcher-list '' 'm:{[:lower:][:upper:]}={[:upper:][:lower:]}'
zstyle ':completition:*' rehash true
zstyle ':completition:*' accept-exact '*(N)'
zstyle ':completition:*' use-cache on
zstyle ':completition:*' cache-path "$XDG_CONFIG_HOME/zsh/cache"
zmodload zsh/complist

_comp_options+=(globdots)

autoload -Uz vcs_info
precmd () {vcs_info}
setopt PROMPT_SUBST

setopt extendedglob
setopt numericglobsort
setopt autocd
setopt combining_chars

bindkey -v

# History
HISTSIZE=100
SAVEHIST=100
HISTFILE=$HOME/.config/zsh/zsh_history

source /usr/share/zsh/plugins/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh

export NVM_DIR="$HOME/.config/nvm"
[ -s "/usr/local/opt/nvm/nvm.sh" ] && \. "/usr/local/opt/nvm/nvm.sh" # This loads nvm
# [ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion"  # This loads nvm bash_completion


# # Load Angular CLI autocompletion.
# source <(ng completion script)
