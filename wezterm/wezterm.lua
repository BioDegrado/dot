---@type Wezterm
local wezterm = require("wezterm")
local config = wezterm.config_builder()

local color_scheme = "tokyonight-storm"
local scheme = wezterm.get_builtin_color_schemes()[color_scheme]

local function push_multiplex_key(keys, keymap)
	local fallback_keymap = {
		key = keymap.key,
		mods = keymap.mods .. "|CTRL",
		action = keymap.action,
	}
	keys[#keys + 1] = keymap
	keys[#keys + 1] = fallback_keymap
end

config.default_prog = { "/opt/homebrew/bin/fish" }
config.enable_wayland = false
config.color_scheme = color_scheme
config.font = wezterm.font_with_fallback({
	"Jetbrains Mono",
	"Noto Sans CJK SC",
	"Symbols Nerd Font",
})
config.font_size = 13

config.enable_tab_bar = true
config.use_fancy_tab_bar = false
config.hide_tab_bar_if_only_one_tab = false
config.tab_bar_at_bottom = true
config.tab_max_width = 25

config.window_background_opacity = 0.8
config.macos_window_background_blur = 100
config.window_decorations = "MACOS_FORCE_ENABLE_SHADOW | RESIZE"

config.window_padding = {
	left = 3,
	right = 3,
	top = 3,
	bottom = 3,
}

config.colors = { background = "black" }

config.hide_mouse_cursor_when_typing = true

local action = wezterm.action
config.leader = { key = "a", mods = "CTRL", timeout_milliseconds = 1000 }
config.keys = {
	{ key = "f", mods = "CMD", action = action.ToggleFullScreen },
	{
		key = "q",
		mods = "CMD",
		action = action.CloseCurrentPane({ confirm = true }),
	},
}
push_multiplex_key(config.keys, {
	key = "a",
	mods = "LEADER",
	action = action.SpawnTab("DefaultDomain"),
})
push_multiplex_key(config.keys, {
	key = "k",
	mods = "LEADER",
	action = action.ActivateTabRelative(-1),
})
push_multiplex_key(
	config.keys,
	{ key = "j", mods = "LEADER", action = action.ActivateTabRelative(1) }
)
push_multiplex_key(config.keys, {
	key = "q",
	mods = "LEADER",
	action = action.CloseCurrentTab({ confirm = true }),
})

wezterm.on("update-right-status", function(window, _)
	local date = wezterm.strftime("%a %b %-d %H:%M:%S %Y ")
	local bat = ""

	for _, b in ipairs(wezterm.battery_info()) do
		bat = "󰁹 " .. string.format("%.0f%%", b.state_of_charge * 100)
	end

	window:set_right_status(wezterm.format({
		{ Text = bat .. "  󰥔 " .. date },
	}))
end)

-- The filled in variant of the < symbol
local SOLID_LEFT_ARROW = wezterm.nerdfonts.pl_right_hard_divider

-- The filled in variant of the > symbol
local SOLID_RIGHT_ARROW = wezterm.nerdfonts.pl_left_hard_divider

-- This function returns the suggested title for a tab.
-- It prefers the title that was set via `tab:set_title()`
-- or `wezterm cli set-tab-title`, but falls back to the
-- title of the active pane in that tab.
local function tab_title(tab_info)
	local title = tab_info.tab_title
	-- if the tab title is explicitly set, take that
	if title and #title > 0 then
		return title
	end
	-- Otherwise, use the title from the active pane
	-- in that tab
	return tab_info.active_pane.title
end

local tab_fmt = " %s%s "
local colors = wezterm.get_builtin_color_schemes()[color_scheme]
local foreground = colors.background
local background = colors.foreground
local tab_format = wezterm.on(
	"format-tab-title",
	function(tab, tabs, panes, conf, hover, max_width)
		local title = tab_title(tab)

		-- ensure that the titles fit in the available space,
		-- and that we have room for the edges.
		title = wezterm.truncate_right(title, max_width - 2)

		return {
			{ Text = string.format(tab_fmt, tab:tab_id(), title) },
		}
	end
)

return config
