#!/bin/sh

windowname="$1"
hvisiblename="$2"

echo "$hvisiblename"

if [ "$(eww windows | grep "*$windowname")" == "" ]; then
	[ "$hvisiblename" != "" ] && eww update "$hvisiblename"=true
	eww open "$windowname"
else
	[ "$hvisiblename" != "" ] && eww update "$hvisiblename"=false
	eww close "$windowname"
fi
