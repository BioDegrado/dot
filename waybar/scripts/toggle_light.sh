#!/bin/sh

if [ "$(light -G)" == 0.00 ]; then
	light -I
else
	light -O
	light -S 0
fi
