#!/bin/sh

function usage () {
	echo "\
This scripts accepts the following parameters:
-d:	device 'input' or 'output'
-p:	Ignore printing the status
-s:	Switch the sinks
-v:	Print status and error messages
-h:	Print this message"
}
function get_vol() {
	local stream="$1"
	# echo "amixer -D pulse sget $stream | grep 'Left:' | grep -o '[0-9]\+%' | tr -d '%'"
	amixer -D pulse sget "$stream" | grep 'Left:' | grep -o '[0-9]\+%' | tr -d '%\n'
}

function mute() {
	local stream="$1"
	amixer -D pulse sset "$stream" toggle  > /dev/null
}

function volume() {
	local stream="$1"
	local mod="$2"
	amixer -D pulse sset "$stream" "$mod" > /dev/null
}

function get_state() {
	local stream="$1"
	local mute="$(amixer -D pulse sget "$stream" | grep 'Left:' | grep -o '\[\(on\|off\)\]' | tr -d '[]')"

	if [ "$mute" == off ]; then
		echo "mute"
	else
		get_vol "$stream"
	fi
}

function get_icon() {
	local stream="$1"
	local state="$(get_state "$stream")"
	if [ "$stream" == "Master" ]; then 
		if [ "$state" == mute ]; then
			echo -n ""
		elif [ "$state" -le "33" ]; then
			echo -n  ""
		elif [ "$state" -le "66" ]; then
			echo -n ""
		else
			echo -n ""
		fi
	elif [ "$stream" == "Capture" ]; then  
		if [ "$state" == mute ]; then
			echo -n "" 
		else
			echo -n ""
		fi
	fi
}

opt="$1"
action=""
stream="Master"
while getopts "d:gmv:ih" opts; do
	case "$opts" in
		d)
			if [ "$OPTARG" == "input" ]; then
				stream=Capture
			elif [ "$OPTARG" == "output" ]; then
				stream=Master
			else
				exit 1
			fi
			;;
		g)
			get_vol "$stream"
			;;
		v)
			volume "$stream" "$OPTARG"
			;;
		m)
			mute "$stream"
			;;
		i)
			get_icon "$stream"
			;;
		h)
			usage
			;;
		*)
			echo "Option invalid"
			exit 1
			;;
	esac
done
