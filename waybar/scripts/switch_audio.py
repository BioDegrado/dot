#!/usr/bin/env python

import re
import json
import time
import argparse
import subprocess
from typing import Any

from pyparsing import (
    Word,
    Combine,
    White,
    ZeroOrMore,
)

IGNORED_PROPS = [
    "Volume",
    "balance",
    "device",
    "Latency",
    "audio",
    "api",
    "Flags",
    "Description",
    "factory",
    "object",
    "priority",
    "alsa",
]


ALSA_SINK = r"alsa_output\.pci-[0-9A-Za-z_]+\.([0-9]+\.)+analog-stereo"
ALSA_SOURCE = r"alsa_input\.pci-[0-9A-Za-z_]+\.([0-9]+\.)+analog-stereo"
USB_DAC_SINK = r"alsa_output.usb-C-Media_Electronics_Inc._USB_PnP_Sound_Device-([0-9]+\.)+analog-stereo"
USB_DAC_SOURCE = r"alsa_input.usb-C-Media_Electronics_Inc._USB_PnP_Sound_Device-([0-9]+\.)+analog-stereo"
PROFILES = [
    {
        "sink": USB_DAC_SINK,
        "source": USB_DAC_SOURCE,
        "icon": "󰋋",
        "port": "analog-output",
    },
    {
        "sink": ALSA_SINK,
        "source": ALSA_SOURCE,
        "icon": "󰓃",
        "port": "analog-output-lineout",
    },
    {
        "sink": ALSA_SINK,
        "icon": "󰋎",
        "port": "analog-output-headphones",
    },
]

SINKS: list[Any] = []
SOURCES: list[Any] = []


def get_spaced_match(chars: str):
    return Combine(Word(chars) + ZeroOrMore(Combine(White(" ") + Word(chars))))


def get_chinese_symbols():
    ranges = [int("4E00", 16), int("9FFF", 16)]
    chinese_symbols = ""
    chinese_symbols += "".join(
        chr(c) for c in range(ranges[0], ranges[1] + 1) if not chr(c).isspace()
    )
    return chinese_symbols


def remove_properties(text: str, properties: list[str]) -> str:
    properties = [rf"\s*{i}" for i in properties]
    query = "|".join(properties)

    regex = re.compile(query)
    listed_text = [line for line in text.split("\n") if not regex.match(line)]
    listed_text = "\n".join(listed_text)
    return listed_text


def get_sink_info(ignore_properties: list[str]):
    info, _ = pactl("list", "sinks", check=False)
    info = remove_properties(info, ignore_properties)
    return info


def get_source_info(ignore_properties: list[str]):
    info, _ = pactl("list", "sources", check=False)
    info = remove_properties(info, ignore_properties)
    return info


def get_audio_state():
    sinks, _ = pactl("--format=json", "list", "sinks", ignore_error=True)
    sources, _ = pactl("--format=json", "list", "sources", ignore_error=True)
    return json.loads(sinks), json.loads(sources)


def pactl(
    *args: str, check: bool = False, ignore_error: bool = False
) -> tuple[str, str]:
    process = subprocess.run(["pactl", *args], capture_output=True)

    if check:
        process.check_returncode()

    out = process.stdout.decode("utf-8")
    err = process.stderr.decode("utf-8")
    if err and not ignore_error:
        _ = subprocess.run(["notify-send", f"switch-audio {args}", err])
    return out, err


def get_running_sink_idx() -> int:
    info, _ = pactl("info")

    info = info.split("\n")
    default_sink = [i for i in info if "Default Sink: " in i][0]
    default_sink = default_sink.removeprefix("Default Sink: ")
    for idx, sink in enumerate(SINKS):
        if sink["properties"]["node.name"] == default_sink:
            return idx
    raise Exception("No sink found!")


def get_profile_idx(sink) -> int:
    sink_name = sink["properties"]["node.name"]
    sink_port = sink["active_port"]
    for idx, profile in enumerate(PROFILES):
        if re.match(profile["sink"], sink_name) and profile["port"] == sink_port:
            return idx

    sinks = "\n\t\t\t".join([i["sink"] for i in PROFILES])
    raise ValueError(
        f"No match for {sink_name}:\n"
        f"\tWith profiles:\n"
        f"\t\t\t{sinks}\n"
        f"\t\tand port: {sink_port}"
    )


def get_data_from_profile(profile_idx: int):
    profile = PROFILES[profile_idx]
    found_sink = None
    for sink in SINKS:
        sink_name = sink["properties"]["node.name"]
        if re.match(profile["sink"], sink_name):
            found_sink = sink

    if found_sink is None:
        sinks = [i["properties"]["node.name"] for i in SINKS]
        sinks = "\n\t\t\t".join(sinks)
        sink = profile["sink"]
        raise ValueError(
            f"No match for {sink}:\n" f"\tWith sinks:\n" f"\t\t\t{sinks}\n"
        )

    if "source" not in profile:
        return found_sink, None

    found_source = None
    for source in SOURCES:
        source_name = source["properties"]["node.name"]
        if re.match(profile["source"], source_name):
            found_source = source

    if found_source is None:
        raise Exception("Source not found!")

    return found_sink, found_source


def activate_profile(profile_idx: int):
    profile = PROFILES[profile_idx]
    source, sink = {}, {}
    try:
        sink, source = get_data_from_profile(profile_idx)
    except ValueError:
        raise ValueError("no profile found")
    sink_name = sink["properties"]["node.name"]
    try:
        _ = pactl("set-default-sink", sink_name, check=True)
    except subprocess.CalledProcessError:
        raise RuntimeError("sink not found!")
    try:
        _ = pactl("set-sink-port", sink_name, profile["port"], check=True)
    except subprocess.CalledProcessError:
        raise RuntimeError("Port not found!")

    if "source" in profile:
        assert source is not None
        source_name = source["properties"]["node.name"]
        _ = pactl("set-default-source", source_name)


def set_next_profile(verbose: bool):
    running_sink = SINKS[get_running_sink_idx()]
    profile_idx = get_profile_idx(running_sink)
    next = (profile_idx + 1) % len(PROFILES)
    if verbose:
        print(f"Trying {len(PROFILES)} profiles.")
    for i, _ in enumerate(PROFILES, start=1):
        next = (profile_idx + i) % len(PROFILES)
        if verbose:
            print(f"Trying profile: {PROFILES[next]['icon']}")
        try:
            activate_profile(next)
        except ValueError as e:
            print(f"Cannot find profile: {e}")
        else:
            break


def get_status_force(tries=5):
    running_sink = None
    for _ in range(tries):
        try:
            running_sink = SINKS[get_running_sink_idx()]
            break
        except subprocess.CalledProcessError:
            pass
        time.sleep(1)
        raise RuntimeError(f"Error: Cannot print status")
    return PROFILES[get_profile_idx(running_sink)]


def get_status(force=False):
    profile = {}
    if force:
        profile = get_status_force()
    else:
        running_sink = SINKS[get_running_sink_idx()]
        profile = PROFILES[get_profile_idx(running_sink)]
    output = json.dumps({"text": profile["icon"]})

    # print(profile["icon"])
    print(output)


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    _ = parser.add_argument(
        "-v",
        "--verbose",
        action=argparse.BooleanOptionalAction,
        help="Print status and error messages.",
    )
    _ = parser.add_argument(
        "-f",
        "--force",
        action=argparse.BooleanOptionalAction,
        help="Tries multiple times to output the status.",
    )
    _ = parser.add_argument(
        "-s",
        "--switch",
        action=argparse.BooleanOptionalAction,
        help="Switch to the next profile and outputs the icon.",
    )
    args = parser.parse_args()

    SINKS, SOURCES = get_audio_state()
    if args.switch:
        set_next_profile(args.verbose)
    get_status(args.force)
