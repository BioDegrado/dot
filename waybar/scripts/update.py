#!/usr/bin/env python

import argparse
import re
import subprocess
import json
from typing import Callable

Cmd = list[str]


def run(cmd: Cmd) -> tuple[str, str]:
    process = subprocess.run(cmd, capture_output=True)
    out = process.stdout.decode("utf-8") if process else ""
    err = process.stderr.decode("utf-8") if process else ""
    return out, err


def run_on_terminal(cmds: list[Cmd], cls: str) -> list[tuple[str, str]]:
    terminal_cmd = [
        "wezterm",
        "start",
        "--always-new-process",
        "--class",
        cls,
    ]
    return [run([*terminal_cmd, *cmd]) for cmd in cmds if cmd]


def cmd_output_lines(cmd: Cmd) -> list[str]:
    out, _ = run(cmd)
    out = out.split("\n")
    return list(filter(None, out))


def pacman_packages() -> list[str]:
    return cmd_output_lines(["checkupdates"])


def yay_packages() -> list[str]:
    return cmd_output_lines(["yay", "-Qua"])


def flatpak_packages() -> list[str]:
    return cmd_output_lines(["flatpak", "remote-ls", "--updates"])


def send_notification(title: str, msg: str) -> int:
    out, _ = run(["dunstify", title, msg, "-p"])
    return int(out)


def close_notification(id: int):
    _ = run(["dunstify", "-C", str(id)])


TOOLTIP_HEADER_FORMAT = (
    """<span weight='bold' color='white'>{}</span> <span color='#74c7ec'>({})</span>"""
)
TOOLTIP_PACKAGE_FORMAT = """<span color='white'>{:<32}</span> <span color='white'>{}</span><span color='#f38ba8'>{}</span> -> <span color='white'>{}</span><span color='#a6e3a1'>{}</span>"""


def get_tooltip(packages: dict[str, list[str]]) -> str:
    packages = {p: e for p, e in packages.items() if len(e)}
    res: list[str] = []
    for pm, pm_packages in packages.items():
        header = TOOLTIP_HEADER_FORMAT.format(pm.upper(), len(pm_packages))
        content = [
            TOOLTIP_PACKAGE_FORMAT.format(
                p["name"], p["same"], p["old_diff"], p["same"], p["new_diff"]
            )
            for p in extract_package_info(pm, pm_packages)
        ]
        content = content if len(content) < 10 else [*content[:10], "..."]
        res = [*res, header, *content]
    return "\n".join(res)


def common_prefix(s1: str, s2: str) -> str:
    v1 = re.split("\\.|-", s1)
    v2 = re.split("\\.|-", s2)
    res: int = 0
    for i, (c1, c2) in enumerate(zip(v1, v2)):
        if c1 != c2:
            break
        res += len(c1) + 1
    return s1[:res]


def extract_package_info(
    packagemanager: str, packages: list[str]
) -> list[dict[str, str]]:
    if packagemanager in ["yay", "pacman"]:
        packages_split_by_space = [p.split() for p in packages]
        return [
            {
                "name": p[0],  # The first element is always the name
                "old": p[1],  # The second element is always the old version
                "new": p[3],  # The fourth element is always the new version
                "same": prefix,
                "old_diff": p[1][len(prefix) :] if len(prefix) <= len(p[1]) else "",
                "new_diff": p[3][len(prefix) :] if len(prefix) <= len(p[3]) else "",
            }
            for p in packages_split_by_space
            if (prefix := common_prefix(p[1], p[3])) != False
        ]
    else:
        NAME = 0
        ID = 1
        VERSION = 2
        BRANCH = 3
        get_version: Callable[[list[str]], str] = lambda e: (
            e[VERSION] if e[VERSION] != "" else e[BRANCH]
        )
        remote_packages = [p.split("\t") for p in packages]
        local_packages = [p.split("\t") for p in cmd_output_lines(["flatpak", "list"])]

        local_packages_dict = {ele[ID]: get_version(ele) for ele in local_packages}
        remote_packages_dict = {ele[ID]: get_version(ele) for ele in remote_packages}
        packages_name = {ele[ID]: ele[NAME] for ele in remote_packages}

        return [
            {
                "name": packages_name[id],
                "old": local_packages_dict[id],
                "new": verr,
                "same": prefix,
                "old_diff": (
                    local_packages_dict[id][len(prefix) :]
                    if len(prefix) <= len(local_packages_dict[id])
                    else ""
                ),
                "new_diff": verr[len(prefix) :] if len(prefix) <= len(verr) else "",
            }
            for id, verr in remote_packages_dict.items()
            if (prefix := common_prefix(verr, local_packages_dict[id])) != False
        ]


def check_packages(silent: bool = False):
    id = -1
    if not silent:
        id = send_notification("PACMAN CHECK FOR UPDATES", "Checking...")

    pac_packs = pacman_packages()
    yay_packs = yay_packages()
    flat_packs = flatpak_packages()

    if not silent:
        if id != -1:
            close_notification(id)
        _ = send_notification("CHECK FINISHED", "Checked!")

    return {
        "pacman": pac_packs,
        "yay": yay_packs,
        "flatpak": flat_packs,
    }


def print_packages(packages: dict[str, list[str]], compact: bool = False):
    pac_up, yay_up, flat_up = (
        len(packages["pacman"]),
        len(packages["yay"]),
        len(packages["flatpak"]),
    )
    nPackages = pac_up + yay_up + flat_up
    if not nPackages:
        state = "uptodate"
        tooltip = "uptodate"
        print(
            json.dumps(
                {"alt": "uptodate", "text": "", "class": state, "tooltip": tooltip}
            )
        )
        return

    if compact:
        out = f"{nPackages}"
        state = "old" if nPackages < 30 else "urgent"
        tooltip = get_tooltip(packages)
        print(
            json.dumps(
                {"alt": "outdated", "text": out, "class": state, "tooltip": tooltip}
            )
        )
        return

    out = ""
    tooltip = ""
    if yay_up:
        out += f"󱉛{yay_up}"
        tooltip += TOOLTIP_HEADER_FORMAT.format("YAY", len(packages["yay"]))
    if pac_up:
        out += f"󰏗{pac_up}"
        tooltip += TOOLTIP_HEADER_FORMAT.format("PACMAN", len(packages["pacman"]))
    if flat_up:
        out += f"{flat_up}"
        tooltip += TOOLTIP_HEADER_FORMAT.format("FLATPAK", len(packages["flatpak"]))
    state = "old" if nPackages < 30 else "urgent"
    print(json.dumps({"alt": "", "text": out, "class": state, "tooltip": tooltip}))


def update_packages():
    _ = run_on_terminal(
        [
            ["sudo", "pacman", "-Syu", "--noconfirm"] if pacman_packages() else [],
            ["yay", "-Syu", "--devel"] if yay_packages() else [],
            ["flatpak", "update", "-y"] if flatpak_packages() else [],
        ],
        "pac_update",
    )


def clean_packages():
    _ = run_on_terminal(
        [
            ["sudo", "pacman", "-Sc"],
            ["yay", "-Sc"],
            ["sudo", "pacman", "-Rs", "$(pacman -Qdtq)"],
        ],
        "pac_clean",
    )


def list_packages():
    _ = run_on_terminal([["bash", "-c", "pacman -Qe | fzf --ansi"]], "pac_clean")


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    _ = parser.add_argument(
        "-u",
        "--update",
        action=argparse.BooleanOptionalAction,
        help="Update the system.",
    )
    _ = parser.add_argument(
        "-l",
        "--list",
        action=argparse.BooleanOptionalAction,
        help="List packages on the system.",
    )
    _ = parser.add_argument(
        "-r",
        "--clean",
        action=argparse.BooleanOptionalAction,
        help="Clean files of the package manager.",
    )
    _ = parser.add_argument(
        "-c",
        "--check",
        action=argparse.BooleanOptionalAction,
        help="Check for updates.",
    )
    _ = parser.add_argument(
        "-C",
        "--check-silent",
        action=argparse.BooleanOptionalAction,
        help="Check for updates and doesn't send notification.",
    )
    _ = parser.add_argument(
        "-s",
        "--compact",
        action=argparse.BooleanOptionalAction,
        help="Print a more compact status",
    )
    args = parser.parse_args()

    if args.update:
        update_packages()
        packages = check_packages(False)
        print_packages(packages, args.compact)
    elif args.check:
        packages = check_packages(False)
        print_packages(packages, args.compact)
    elif args.check_silent:
        packages = check_packages(True)
        print_packages(packages, args.compact)
    elif args.list:
        list_packages()
    elif args.clean:
        clean_packages()
