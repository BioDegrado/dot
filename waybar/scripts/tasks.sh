#!/bin/sh

quotestring ()
{
	echo -n "\"$@\""
}

parse_date ()
{
	[ "$1" == "null" ] && return 0
	local out="$(echo "$1" | sed -e 's/\([0-9]\{4\}\)\([0-9]\{2\}\)\([0-9]\{2\}\)T\([0-9]\{2\}\)\([0-9]\{2\}\)/\1-\2-\3T\4:\5:/')"
	date -d"$out" +'%y/%m/%d %H:%M'
}

gettask ()
{
	local id="$1"
	local description="$(quotestring "$2")"
	local project="$(quotestring "$3")"
	local duedate="$(quotestring "$4")"
	local scheduled="$(quotestring "$5")"

	local result="(task :description ${description} :id ${id}"
	[ "$project" != '"null"' ] && result+=" :project ${project}"
	[ "$duedate" != '"null"' ] && result+=" :duedate ${duedate}"
	[ "$scheduled" != '"null"' ] && result+=" :scheduled ${scheduled}"
	result+=")"
	echo -n "$result"
}

gettasks ()
{
	first=true
	json_query='.[] | select(.status == "pending") | {id: .id, description: .description, due: .due, project: .project, scheduled: .scheduled}'
	task export | jq -cr "$json_query" | while read ele; do
		if [ "$first" == true ]; then
			first=false
		else
			id="$(echo -n "$ele" | jq -cr '.id')"
			description="$(echo -n "$ele" | jq -cr '.description')"
			duedate="$(parse_date "$(echo -n "$ele" | jq -cr '.due')")"
			scheduled="$(parse_date "$(echo -n "$ele" | jq -cr '.scheduled')")"
			project="$(echo -n "$ele" | jq -cr '.project')"
				gettask "$id" "$description" "$project" "$duedate" "$scheduled"
		fi
	done
}

echo "(box :spacing 5 :class 'bar' :orientation \"v\" $(gettasks))"
