#!/bin/bash

PATH_DEST='/tmp'
album=$(playerctl metadata --format '{{mpris:artUrl}}')

	if [[ -z "$album" ]] || [[ $album == '' ]]; then
		exit 0
	fi

	# Album is from an URL online
	if [[ "$album" != 'file://'* ]]; then
		curl --silent "$album" --output "$PATH_DEST/album_art"
		echo "$PATH_DEST/album_art"
		exit 0
	fi

	# Album in local path
	album="${album//file:\/\//}"
	filename=$(basename "$album")
	extension=${filename##*.}
	path="$PATH_DEST/album_art.$extension"
	cp "$album" "$path"
	echo "$path"
