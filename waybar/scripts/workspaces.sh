#!/bin/bash
#
monitor="$1"
trap init_states USR1

function array_contains() {
	local declare -a arr=("${!1}")
	local obj="$2"
	for ele in "${arr[@]}"; do
		if [ "$obj" == "$ele" ]; then
			return 0
		fi
	done
	return 1
}
function init_states() {
	read -d '\0' -a busy_workspaces <<< "$(hyprctl -j workspaces | jq '.[] | {id, windows} | select(.windows > 0) | .id')"
	for i in "${busy_workspaces[@]}"; do
		set_busy "$i"
	done
	
	# NOTE: Get the active workspace. It is marked as old since it will contain the old workspace in the cycle.
	old_workspace="$(hyprctl -j activewindow | jq '.workspace.id')"
	set_active "$old_workspace"
}


function is_busy() {
	local workspace="$1"
	local res="$(hyprctl -j workspaces | jq ".[] | {id, windows} | select(.windows > 0 and .id == $workspace)")"
	# echo "res: $res"
	if [ "$res" == "" ]; then
		return 1
	fi
	return 0
}

function is_active() {
	local workspace="$1"
	local res="$(hyprctl -j activeworkspace | jq ".id")"
	# echo "res: $res and $workspace"
	if [ "$res" = "$workspace" ]; then
		# echo "workspace active: $workspace"
		return 0
	fi
	return 1
}

# Sets the state of the workspace to busy if it is inactive
# param 1: workspace id
function set_busy() {
	[ "$1" = "" ] && ( echo "wtf"; return 1) 

	local state="$(eww get sw"$1")" 
	if [ "busy" = "$state" ] ; then
		# echo "state is not inactive: $state"
		return 1
	fi
	eww update "sw$1=busy"
}
#
# param 1: workspace id
function set_active() {
	eww update "sw$1=active"
}

# param 1: workspace id
function set_inactive() {
	eww update "sw$1=inactive"
}

function deactivate_workspace() {
	local workspace="$1"
	if is_busy "$workspace"; then
		set_busy "$workspace"
	else
		set_inactive "$workspace"
	fi
}

function change_workspace() {
	set_active "$1"
	deactivate_workspace "$2"
}

# NOTE: Wait for eww to be initialized, not very good, but hey, it works
while ! pgrep eww; do
	sleep 1
done

busy_workspaces=""
old_workspace=""
init_states

socat - "UNIX-CONNECT:/tmp/hypr/$HYPRLAND_INSTANCE_SIGNATURE/.socket2.sock" | while read line; do
	read -a line <<< "$(echo "$line" | tr '>>' ' ')"
	event="${line[0]}"
    # echo "socket output: ${line[@]}"
	if [ "$event" == "workspace" ]; then
		# echo "changing workspace: $event"
		workspace="${line[1]}"
		change_workspace "$workspace" "$old_workspace"
		old_workspace="$workspace"

	elif [ "$event" == "destroyworkspace" ]; then
		# echo "destroing workspace: $event"
		workspace="${line[1]}"
		deactivate_workspace "$workspace"

	elif [ "$event" == "openwindow" ]; then
		# echo "opening window: $event"
		read -d '\0' -a params <<< "$(echo "${line[1]}" | tr ',' '\n')"
		# echo "params: ${params[@]}"
		workspace="${params[1]}"
		# echo "workspace: $workspace"
		if ! is_active "$workspace"; then
			set_busy "$workspace"
		fi
	fi
done
echo "The End"
