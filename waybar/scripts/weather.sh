#!/bin/bash

CITY=Turin

get_temp() {
	local weather_obj="$1"
	echo "$weather_obj" | jq '.avgtempC' | tr -d '"' | xargs printf "%2.f"
}

get_weather_icon() {
	local weather_obj="$1"
	echo "$weather_obj" | jq '.weatherDesc' | tr -d '"'
}


weather="$(curl -s "v2.wttr.in/${CITY}?format=j2")"
err="$?"
if [[ "$err" != 0 ]]; then 
	echo "{\"icon\": \"󰛉\", \"text\":\"\", \"class\":\"error\"}"
	exit 1
fi

output="$(curl --show-error --get --fail --silent "v2n.wttr.in/${CITY}?format=%c")"
err="$?"
# Sometimes the endpoint doesn't return an error code, instead gives an error message
# with 200 OK
if [[ "$err" != 0 ]] || [[ "$output" == *"Unknown location"* ]]; then 
	echo "{\"text\":\"󰛉\", \"tooltip\":\"$output\", \"class\": \"error\"}"
	exit 0
else
	output=$(echo "$output" | xargs)
	output+="\\n"
fi

length=$(echo "$weather" | jq '.weather | length')
for ((i=0;i<length;i++)); do
	weather_obj=$(echo "$weather" | jq ".weather[$i]")
	temp=$(get_temp "$weather_obj")
	# icon=$(get_weather_icon "$weather_obj")
	output+="$temp"
	if [[ "$i" != "$((length - 1))" ]]; then
		output+="\\n"
	fi
done

tooltip="$(curl -fGSs "https://wttr.in/${CITY}?2T")"
tooltip=${tooltip//\\/\\\\\\\\}
tooltip=${tooltip//$'\n'/\\n}
tooltip=${tooltip//\"/\\\"}
echo "{\"text\":\"$output\", \"tooltip\":\"$tooltip\"}"
